﻿namespace Server
{
    public enum ServerStatusEnum
    {
        Working = 1,
        Restarting = 2,
        Stopping = 3,
        Stopped = 4,
        Error = 5
    }
}
