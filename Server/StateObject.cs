﻿namespace Server
{
    using System.Net.Sockets;

    class StateObject
    {
        public Socket WorkSocket { get; set; }
        public const int BUFFER_SIZE = sizeof(int);
        public byte[] Buffer = new byte[BUFFER_SIZE];
    }
}
