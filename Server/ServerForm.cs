﻿namespace Server
{
    using System;
    using System.Windows.Forms;

    public partial class ServerForm : Form
    {
        private Server _server;
        public ServerForm()
        {
            InitializeComponent();
            Server.SetStatusBox(txtStatus);
            _server = new Server();
            int port = Convert.ToInt32(txtPort.Text);
            _server.Start(port);
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            int port = Convert.ToInt32(txtPort.Text);
            _server.Restart(port);
        }

        private void ServerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _server.Stop();
        }
    }
}
