﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Server
{
    public class Server
    {
        private Socket _socket;
        private static ManualResetEvent _allDone = new ManualResetEvent(false);
        private static TextBox _statusBox;
        private Thread _mainThread;
        private static ServerStatusEnum serverStatus = ServerStatusEnum.Stopped;
        private const string EOF = "EOF";

        public Server()
        {
            ChangeStatus(ServerStatusEnum.Stopped);
        }

        public static void SetStatusBox(TextBox statusBox)
        {
            _statusBox = statusBox;
        }

        public void Start(int port)
        {
            try
            {
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _mainThread = new Thread(new ParameterizedThreadStart(ThreadWork));
                _mainThread.Start(port);
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        public void Stop()
        {
            try
            {
                if (_mainThread != null && _mainThread.IsAlive)
                {
                    ChangeStatus(ServerStatusEnum.Stopping);
                    _allDone.Set();
                    _mainThread.Join(1000);
                    _socket.Close();
                }
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        public void Restart(int port)
        {
            ChangeStatus(ServerStatusEnum.Restarting);
            Stop();
            Start(port);
        }

        private static void ChangeStatus(ServerStatusEnum status)
        {
            serverStatus = status;
            if (_statusBox.InvokeRequired)
            {
                _statusBox.Invoke(new Action<string>((s) => _statusBox.Text = s), status.ToString());
            }
            else {
                _statusBox.Text = status.ToString();
            }
        }

        private void ThreadWork(object port)
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, (int)port);
                _socket.Bind(localEndPoint);
                _socket.Listen(100);
                ChangeStatus(ServerStatusEnum.Working);

                while (serverStatus == ServerStatusEnum.Working)
                {
                    _allDone.Reset();
                    _socket.BeginAccept(new AsyncCallback(AcceptCallback), _socket);
                    _allDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        private static void AcceptCallback(IAsyncResult result)
        {
            try
            {
                if (serverStatus == ServerStatusEnum.Working)
                {
                    _allDone.Set();
                    Socket listener = (Socket)result.AsyncState;
                    Socket client = listener.EndAccept(result);

                    StateObject state = new StateObject();
                    state.WorkSocket = client;
                    client.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(ReadCallback), state);
                }
            }
            catch (ObjectDisposedException)
            {
                // This error has occured then socket is closed. It's usually happened then server is restarting.
                // Nothing to do, just skip.
                return;
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        private static void ReadCallback(IAsyncResult result)
        {
            try
            {
                StateObject state = (StateObject)result.AsyncState;
                Socket client = state.WorkSocket;
                int bytesRead = client.EndReceive(result);
                if (bytesRead > 0)
                {
                    int id = BitConverter.ToInt32(state.Buffer, 0);
                    var requestedPerson = Person.People.Where(x => x.ID == id).SingleOrDefault();
                    var data = SerializePerson(requestedPerson);
                    Send(client, data);
                    Send(client, Encoding.ASCII.GetBytes(EOF));
                }
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        private static byte[] SerializePerson(Person person)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Person));
            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, person);
                return ms.GetBuffer();
            }
        }

        private static void Send(Socket socket, byte[] data)
        {
            socket.BeginSend(data, 0, data.Length, 0, null, socket);
        }

        private static void ShowErrorMessage(Exception e)
        {
            ChangeStatus(ServerStatusEnum.Error);
            MessageBox.Show(e.Message, "Error!");
        }
    }
}
