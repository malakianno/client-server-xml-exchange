﻿namespace Client
{
    using System;
    using System.Net;
    using System.Text;
    using System.Windows.Forms;

    public partial class ClientForm : Form
    {
        private Client _client;
        public ClientForm()
        {
            InitializeComponent();
            Client.SetOutputBox(txtResponse);
            _client = new Client();
        }

        private void btnSendRequest_Click(object sender, EventArgs e)
        {
            txtResponse.Text = string.Empty;
            if (!ValidateUI())
            {
                return;
            }
            var splittedAddress = IpAddressTextBox.Text.Split(':');
            var hostname = splittedAddress[0];
            var port = Convert.ToInt32(splittedAddress[1]);
            var data = BitConverter.GetBytes(Convert.ToInt32(txtID.Text));
            IPHostEntry ipHostInfo = Dns.Resolve(hostname);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
            _client.SendRequest(remoteEP, data);
        }

        private bool ValidateUI()
        {
            var splittedAddress = IpAddressTextBox.Text.Split(':');
            if (splittedAddress.Length != 2)
            {
                MessageBox.Show("Server address is incorrect (ip:port)");
                return false;
            }
            try
            {
                Dns.Resolve(splittedAddress[0]);
            }
            catch (Exception)
            {
                MessageBox.Show("Ip address is incorrect");
                return false;
            }
            try
            {
                Convert.ToInt32(splittedAddress[1]);
            }
            catch (Exception)
            {
                MessageBox.Show("Port is incorrect");
                return false;
            }
            if (txtID.Text == string.Empty)
            {
                MessageBox.Show("ID cannot be empty");
                return false;
            }
            try
            {
                Convert.ToInt32(txtID.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("ID must be number");
                return false;
            }
            if (Convert.ToInt32(txtID.Text) < 1 || Convert.ToInt32(txtID.Text) > 10)
            {
                MessageBox.Show("ID must be from 1 to 10 inclusive");
                return false;
            }
            return true;
        }
    }
}
