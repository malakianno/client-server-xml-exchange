﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace Client
{
    public class Client
    {
        private static TextBox _outputBox;
        private static ManualResetEvent _connectDone;
        private static ManualResetEvent _sendDone;
        private static ManualResetEvent _receiveDone;
        private const string EOF = "EOF";

        public Client()
        {
            _connectDone = new ManualResetEvent(false);
            _sendDone = new ManualResetEvent(false);
            _receiveDone = new ManualResetEvent(false);
        }

        public static void SetOutputBox(TextBox outputBox)
        {
            _outputBox = outputBox;
        }

        public void SendRequest(IPEndPoint remoteEP, byte[] data)
        {
            try
            {
                var socket = CreateSocket();
                if (Connect(socket, remoteEP))
                {
                    Send(socket, data);
                    Receive(socket);
                    Close(socket);
                }
                else
                {
                    throw new Exception("Connection is failed");
                }
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        private Socket CreateSocket()
        {
            _connectDone.Reset();
            _sendDone.Reset();
            _receiveDone.Reset();
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        private bool Connect(Socket socket, IPEndPoint remoteEP)
        {
            var connectionResult = socket.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), socket);
            connectionResult.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(5));
            _connectDone.WaitOne();
            return socket.Connected;
        }

        private void Send(Socket socket, byte[] data)
        {
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
            _sendDone.WaitOne();
        }

        private void Receive(Socket socket)
        {
            try
            {
                StateObject state = new StateObject();
                state.WorkSocket = socket;
                state.Data = new StringBuilder();

                socket.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0,
                    new AsyncCallback(ReceiveCallback), state);
                _receiveDone.WaitOne();
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        private void Close(Socket socket)
        {
            socket.Close();
        }

        private static void ConnectCallback(IAsyncResult result)
        {
            try
            {
                Socket client = (Socket)result.AsyncState;
                client.EndConnect(result);
                _connectDone.Set();
            }
            catch (SocketException e)
            {
                // Connection refused
                if (e.ErrorCode == 10061)
                {
                    // release the main thread
                    _connectDone.Set();
                }
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        private static void SendCallback(IAsyncResult result)
        {
            try
            {
                Socket client = (Socket)result.AsyncState;
                client.EndSend(result);
                _sendDone.Set();
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        private static void ReceiveCallback(IAsyncResult result)
        {
            try
            {
                StateObject stateObj = (StateObject)result.AsyncState;
                Socket socket = stateObj.WorkSocket;
                int bytesRead = socket.EndReceive(result);
                if (bytesRead > 0)
                {
                    stateObj.Data.Append(Encoding.ASCII.GetString(stateObj.Buffer, 0, bytesRead));
                }

                var response = stateObj.Data.ToString();
                if (response.EndsWith(EOF))
                {
                    _receiveDone.Set();
                    response = response.Substring(0, response.Length - EOF.Length);
                    var xmlDocument = CreateDocument(response);
                    ShowXml(xmlDocument);
                }
                else {
                    socket.BeginReceive(stateObj.Buffer, 0, StateObject.BUFFER_SIZE, SocketFlags.None, new AsyncCallback(ReceiveCallback), stateObj);
                }
            }
            catch (Exception e)
            {
                ShowErrorMessage(e);
            }
        }

        private static XmlDocument CreateDocument(string text)
        {
            XmlDocument result = new XmlDocument();
            using (MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(text)))
            {
                result.Load(ms);
            }

            return result;
        }

        private static void ShowXml(XmlDocument document)
        {
            if (_outputBox.InvokeRequired)
            {
                _outputBox.Invoke(new Action<string>((s) => _outputBox.Text = s), document.InnerXml);
            }
            else {
                _outputBox.Text = document.InnerXml;
            }
        }

        private static void ShowErrorMessage(Exception e)
        {
            MessageBox.Show(e.Message, "Error!");
        }
    }
}
