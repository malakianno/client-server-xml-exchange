﻿namespace Client
{
    using System.Net.Sockets;
    using System.Text;

    class StateObject
    {
        public Socket WorkSocket { get; set; }
        public StringBuilder Data { get; set; }
        public const int BUFFER_SIZE = 1024 * 512;
        public byte[] Buffer = new byte[BUFFER_SIZE];
    }
}
